Paulo Meirelles é professor do Bacharelado em Engenharia de Software da UnB.
Possui doutorado em Ciência da Computação pelo IME-USP (2013); é
pesquisador-colaborador do Centro de Competência em Software Livre (CCSL) e do
Núcleo de Apoio às Pesquisas em Software Livre (NAP-SOL) da USP; coordena a
Evolução do Portal do Software Público Brasileiro, no LAPPIS/UnB; e é consultor
do projeto Participa.Br.
