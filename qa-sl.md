# Práticas de Controle de Qualidade em Projetos de Software Livre

Quem ainda não tem contato cotidiano com projetos de software livre
provavelmente se questiona quanto à qualidade desses projetos. Como é possível
desenvolvedores distribuídos, sem vínculos contratuais entre si e talvez até
voluntários, produzirem software de qualidade?

Projetos de software livre são bastante heterogêneos sob vários aspectos, e a
qualidade é um deles: um projeto de software livre pode ter um processo de
controle de qualidade (normalmente abreviado para QA, do inglês _Quality
Assurance_) definido e bastante eficiente, ou pode não demonstrar nenhuma
preocupação com o assunto.

A incerteza na alocação de recursos para QA não é exclusiva do software livre.
Em projetos privados, frequentemente atividades de QA são preteridas pela
gestão em função de orçamento, prazos ou priorização. Em projetos de software
livre, a abundância ou a escassez de atividades de QA pode estar relacionada a
diferentes fatores, como tamanho do projeto (projetos maiores necessitam mais
de QA), popularidade do projeto (mais participantes tornam mais fácil
implementar QA) e criticidade do projeto para organizações formais (empresas ou
governos podem alocar recursos próprios para QA em projetos de seu interesse).

O contexto da maioria dos projetos de software livre também é diferente de
projetos privados, o que faz com que atividades de QA tenham características um
pouco diferentes. Num projeto privado, a equipe de QA pode ter autoridade,
delegada pela gerência, para demandar trabalho da equipe de desenvolvimento.
Num projeto de software livre, a equipe de QA dificilmente vai poder dizer aos
desenvolvedores o que fazer, limitando-se a relatar os problemas que poderão ou
não ser resolvidos. Numa empresa, é comum haver uma equipe dedicada
exclusivamente para QA, enquanto que na maioria dos projetos de software livre as
pessoas envolvidas com QA provavelmente também desempenham outros papéis no
projeto, algo também comum em equipes que adotam métodos agéis, por exemplo.

Diversas práticas de QA são comuns em projetos de software livre, em especial
nos bem sucedidos. Tais projetos são bem sucedidos _porque_ adotam práticas de
QA, que tornam possível a participação de colaboradores distribuídos
geograficamente. Sua adoção também viabiliza o crescimento sustentável de tais
projetos, que nascem com um pequeno número de colaboradores e podem se
transformar em comunidades com centenas ou milhares de desenvolvedores.

<!------------------------------------------------------------------------>

**Revisão de código**
é uma prática bastante difundida na comunidade de software livre. Comunidades
tradicionais como a do (kernel) Linux e do (servidor web) Apache HTTPD
difundiram esta prática: nessas comunidades, toda mudança a ser realizada no
código-fonte precisa ser revisada por outros desenvolvedores. No caso do Linux,
a revisão é feita pelo mantenedor responsável pelo subsistema afetado antes da
mudança ser incorporada. No Apache HTTPD, ocorrem tanto a revisão _a priori_
para colaboradores externos ao projeto quanto _a posteriori_ para seus membros
oficiais.

Com a popularização do _GitHub_ e de outras plataformas de desenvolvimento
colaborativo "social", esta prática se difundiu bastante, não só em projetos de
software livre, como também em projetos privados. No _GitHub_, colaboradores
podem criar cópias do repositório (_forks_) de qualquer projeto, realizar
modificações e enviar solicitações (_pull requests_) para que as mudanças sejam
incorporadas à versão oficial. Os mantenedores do projeto ou outros
desenvolvedores podem revisar as mudanças, fazer sugestões e críticas e,
eventualmente, incorporá-las.

<!------------------------------------------------------------------------>

**Testes automatizados**
são bastante comuns em projetos de software livre, e dificilmente encontraremos
um projeto bem sucedido sem um bom conjunto de testes automatizados. Devido às
diversas ferramentas existentes, a prática de incluir testes automatizados em
projetos se torna mais comum a cada dia. Uma vez escritos, os testes
automatizados funcionam como um mecanismo de segurança durante o processo de
manutenção: qualquer mudança que faça os testes falharem produz automaticamente
um sinal de alerta para a equipe.

Em projetos com testes automatizados, é comum que os mantenedores solicitem que
as contribuições com novas funcionalidades ou conserto de defeitos venham
acompanhadas dos testes automatizados correspondentes. Além disso, os testes
são utilizados durante o processo de revisão de código: contribuições que fazem
os testes falharem sem uma boa justificativa provavelmente não serão aceitas.

<!------------------------------------------------------------------------>

**Integração contínua**
é também uma prática importante, e consiste na utilização de um serviço que
constantemente executa os testes de um projeto e fornece relatórios com os
resultados.

A comunidade _Perl_ é um bom exemplo de como a integração contínua pode ser
levada a sério. Cada nova versão de um módulo publicado no _CPAN_ (repositório
de módulos Perl) é automaticamente testada em diferentes versões da linguagem e
sistemas operacionais por um cluster distribuído mantido pela comunidade,
chamado _CPAN Testers_. Um ponto interessante é que essa bateria de testes é
realizada sem necessidade de ação explícita do mantenedor de um módulo, ou
seja, os testes associados a um módulo serão executados automaticamente.

Um sistema chamado _Travis CI_ (cujos componentes também são software livre)
vem se popularizando ao fornecer um serviço gratuito de integração contínua
para projetos de software livre hospedados no _GitHub_.  Depois de configurado,
ele executará os testes do projeto sempre que uma nova mudança ocorrer e para
cada novo _pull request_, adicionando um comentário com os resultados dos
testes. O _Travis CI_ possui também uma versão comercial para projetos
privados.

<!------------------------------------------------------------------------>

**Análises estática e dinâmica de código**
também são bastante utilizadas em projetos de software livre, com o objetivo de
identificar potenciais defeitos e garantir conformidade com padrões.

A comunidade _Python_, por exemplo,  produziu uma especificação de convenções
de codificação, chamada _PEP-8_, que serve como um guia para tornar programas
em Python mais legíveis e uniformes entre si. A _PEP-8_ facilita, entre outras
coisas, a colaboração de desenvolvedores em múltiplos projetos com o menor
esforço adicional possível. Existem diversas ferramentas que checam código-fonte
em relação à _PEP-8_, entre elas `pep8`, `pyflakes`, e `flake8`.

Ferramentas de análise dinâmica são também ubíquas em projetos de software
livre: `gdb` (_debugger_), _valgrind_ (analisador de desempenho, uso de memória
e de condições de contorno) e _strace_ (analisador de chamadas de sistema),
apenas para citar algumas, são bastante populares entre os desenvolvedores.

Na comunidade _Debian_, que desenvolve um sistema operacional (ou "distribuição
Linux") de mesmo nome, existe um documento de política técnica chamado de
_Debian Policy_. Esse documento descreve regras e recomendações de como pacotes
devem ser organizados e se comportar, de forma que seja possível uma comunidade
com mais de 1.000 desenvolvedores geograficamente distribuídos manter mais de
20.000 pacotes consistentes entre si e integrados ao sistema. Para garantir a
qualidade dos pacotes _Debian_, checando também, mas não apenas, a sua
conformidade com a _Debian Policy_, são amplamente usadas tanto ferramentas de
análise estática como de análise dinâmica desenvolvidas especialmente para o
projeto.

<!-- CONCLUSÕES ---------------------------------------------------------->

**Conclusões.**
Neste artigo, fizemos uma breve apresentação de algumas das principais práticas
de controle de qualidade em software, apresentando uma pequena parte do que é
usado em projetos de software livre.

Para estudantes e profissionais, participar de atividades de manutenção e QA em
projetos de software livre, certamente, é uma experiência bastante
enriquecedora. Desenvolvedores com histórico de colaboração em projetos de
software livre têm bastante facilidade para encontrar bons trabalhos em muitas
organizações no Brasil e — principalmente — em outros países (trabalhando
remotamente ou presencialmente).

Para organizações cujo negócio depende de projetos de software livre, dedicar
parte do tempo de seus profissionais para atividades de QA neles é uma forma de
garantir a sustentabilidade desses projetos, e, por consequência, a sua
própria.

No caso de organizações desenvolvedoras de software em geral, incentivar seus
profissionais a participar de projetos de software livre durante o expediente
traz diversos benefícios. Não só esses profissionais estarão se aperfeiçoando,
como também poderão trazer práticas e ferramentas de QA validados em projetos
de software livre, ou seja, "no mundo real", para uso em seus projetos
privados.
