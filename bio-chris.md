Christina Chavez é
Professora Associada do Departamento de Ciência da Computação da Universidade Federal da Bahia (UFBA)
e Coordenadora do Programa de Pós-graduação em Ciência da Computação (PGCOMP-UFBA). 
Possui doutorado em Informática pela Pontifícia Universidade Católica do Rio de Janeiro (2004).
Tem experiência na área de Ciência da Computação, com ênfase em Engenharia de Software, 
atuando principalmente nos seguintes temas: 
arquitetura de software (documentação, recuperação e avaliação), evolução de software e 
educação em engenharia de software.

